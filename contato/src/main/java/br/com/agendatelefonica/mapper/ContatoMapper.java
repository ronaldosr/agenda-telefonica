package br.com.agendatelefonica.mapper;

import br.com.agendatelefonica.controller.request.ContatoRequest;
import br.com.agendatelefonica.controller.response.ContatoResponse;
import br.com.agendatelefonica.dataprovider.model.Contato;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ContatoMapper {

    public Contato converterParaContato(ContatoRequest contatoRequest) {
        Contato contato = new Contato();
        contato.setNome(contatoRequest.getNome());
        contato.setTelefone(contatoRequest.getTelefone());
        return contato;
    }

    public ContatoResponse converterParaContatoResponse(Contato contato) {
        ContatoResponse contatoResponse = new ContatoResponse();
        contatoResponse.setId(contato.getId());
        contatoResponse.setUsuarioId(contato.getUsuarioId());
        contatoResponse.setNome(contato.getNome());
        contatoResponse.setTelefone(contato.getTelefone());
        return contatoResponse;
    }

    public List<ContatoResponse> converterParaListaContatoResponse(List<Contato> listaContatosPorUsuarioLogado) {
        List<ContatoResponse> listaContatoResponse = new ArrayList<>();

        for (Contato contato : listaContatosPorUsuarioLogado) {
            ContatoResponse contatoResponse = new ContatoResponse();
            contatoResponse.setId(contato.getId());
            contatoResponse.setUsuarioId(contato.getUsuarioId());
            contatoResponse.setNome(contato.getNome());
            contatoResponse.setTelefone(contato.getTelefone());
            listaContatoResponse.add(contatoResponse);
        }
        return listaContatoResponse;
    }
}
