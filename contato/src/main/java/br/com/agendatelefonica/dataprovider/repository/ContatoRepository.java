package br.com.agendatelefonica.dataprovider.repository;

import br.com.agendatelefonica.dataprovider.model.Contato;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ContatoRepository extends JpaRepository<Contato, Long> {
    List<Contato> findAllByUsuarioId(long id);
}
