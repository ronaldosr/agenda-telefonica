package br.com.agendatelefonica.gateway.decoder;

import br.com.agendatelefonica.gateway.exception.UsuarioLogadoNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class UsuarioLogadoDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 404) {
            return new UsuarioLogadoNotFoundException();
        }
        return errorDecoder.decode(s, response);
    }
}
