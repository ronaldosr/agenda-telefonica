package br.com.agendatelefonica.gateway.config;

import br.com.agendatelefonica.gateway.decoder.UsuarioLogadoDecoder;
import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class UsuarioLogadoGatewayConfiguration {

    @Bean
    public ErrorDecoder getErrorDecoder() {
        return new UsuarioLogadoDecoder();
    }
}
