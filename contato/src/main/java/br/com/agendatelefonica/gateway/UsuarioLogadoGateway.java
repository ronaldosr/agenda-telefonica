package br.com.agendatelefonica.gateway;

import br.com.agendatelefonica.gateway.config.UsuarioLogadoGatewayConfiguration;
import br.com.agendatelefonica.gateway.data.UsuarioData;
import feign.Headers;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(name = "usuariologado", configuration = UsuarioLogadoGatewayConfiguration.class)
public interface UsuarioLogadoGateway {

    @GetMapping("/logado")
    @Headers("Content-Type: application/json")
    UsuarioData consultarUsuarioLogado();
}
