package br.com.agendatelefonica.service;

import br.com.agendatelefonica.dataprovider.model.Contato;
import br.com.agendatelefonica.dataprovider.repository.ContatoRepository;
import br.com.agendatelefonica.gateway.UsuarioLogadoGateway;
import br.com.agendatelefonica.gateway.data.UsuarioData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContatoService {

    @Autowired
    private ContatoRepository contatoRepository;

    @Autowired
    private UsuarioLogadoGateway usuarioLogadoGateway;

    public Contato cadastrarContato(Contato contato) {
        try {
            return contatoRepository.save(contato);
        } catch (DataAccessException e) {
            throw new RuntimeException("Erro ao cadastrar contato: " + e, e);
        }
    }

    public List<Contato> listarContatosPorUsuarioLogado() {
        try {
            UsuarioData usuarioData = usuarioLogadoGateway.consultarUsuarioLogado();
            List<Contato> listaContatos = contatoRepository.findAllByUsuarioId(usuarioData.getId());
            if (listaContatos.isEmpty()) {
                throw new RuntimeException("Não há contatos para o usuário com id " + usuarioData.getId());
            }
            return listaContatos;
        } catch (RuntimeException e) {
            throw new RuntimeException("Erro ao consultar a lista de contatos: " + e, e);
        }
    }

}
