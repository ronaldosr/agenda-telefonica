package br.com.agendatelefonica.controller;

import br.com.agendatelefonica.controller.request.ContatoRequest;
import br.com.agendatelefonica.controller.response.ContatoResponse;
import br.com.agendatelefonica.dataprovider.model.Contato;
import br.com.agendatelefonica.mapper.ContatoMapper;
import br.com.agendatelefonica.security.Usuario;
import br.com.agendatelefonica.service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.persistence.GeneratedValue;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/contatos")
public class ContatoController {

    @Autowired
    private ContatoService contatoService;

    @Autowired
    private ContatoMapper contatoMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ContatoResponse cadastrarContato(@Valid @RequestBody ContatoRequest contatoRequest, @AuthenticationPrincipal Usuario usuario) {
        Contato contato = contatoMapper.converterParaContato(contatoRequest);
        contato.setUsuarioId(usuario.getId());
        return contatoMapper.converterParaContatoResponse(contatoService.cadastrarContato(contato));
    }

    @GetMapping
    public List<ContatoResponse> consultarContatos() {
        return contatoMapper.converterParaListaContatoResponse(contatoService.listarContatosPorUsuarioLogado());
    }

}
