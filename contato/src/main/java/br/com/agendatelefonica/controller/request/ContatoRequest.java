package br.com.agendatelefonica.controller.request;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ContatoRequest {

    @NotNull(message = "Informe o nome do contato")
    private String nome;

    @NotNull(message = "Informe o número do telefone")
    @Size(min = 15, max = 15, message = "O telefone dever seguir o formato '(XX) 91234-5678'")
    private String telefone;

    public ContatoRequest() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
}
