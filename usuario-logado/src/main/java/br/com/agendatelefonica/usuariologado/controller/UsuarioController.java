package br.com.agendatelefonica.usuariologado.controller;


import br.com.agendatelefonica.usuariologado.controller.Usuario;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsuarioController {

    @GetMapping("/logado")
    public Usuario consultarUsuarioLogado (@AuthenticationPrincipal Usuario usuario) {
        usuario.setId(usuario.getId());
        usuario.setName(usuario.getName());
        return usuario;
    }

}
