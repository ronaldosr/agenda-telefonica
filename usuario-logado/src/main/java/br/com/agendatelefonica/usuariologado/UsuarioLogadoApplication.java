package br.com.agendatelefonica.usuariologado;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UsuarioLogadoApplication {

	public static void main(String[] args) {
		SpringApplication.run(UsuarioLogadoApplication.class, args);
	}

}
